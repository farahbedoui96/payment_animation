import 'package:get_it/get_it.dart';

import 'core/Viewmodels/card_list_model.dart';
import 'core/Viewmodels/card_model.dart';


GetIt locator= GetIt.instance;

Future setupLocator() async{
//  var instance = await LocalStorageService.getInstance();
//  locator.registerSingleton<LocalStorageService>(instance);

//  locator.registerLazySingleton(() => AuthenticationService());
//  locator.registerLazySingleton(() => Api());
//  locator.registerFactory(() => UploadViewModel());
  locator.registerLazySingleton(() => CardListModelView());
  locator.registerLazySingleton(() => CardModel()) ;



}