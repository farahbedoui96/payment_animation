import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:payment_animation/ui/views/card_type.dart';
import './views/PaymentPage.dart';
import 'views/walletpage.dart';
import 'views/card_create.dart';
import 'views/card_wallet.dart';



const String initialRoute = "login";

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/cardList':
        return MaterialPageRoute(builder: (_) => WalletPage());
      case '/cardCreate':
        return MaterialPageRoute(builder: (_) => CardCreate());
      case '/cardWallet':
        return MaterialPageRoute(builder: (_) => CardWallet());
      case '/cardType':
        return MaterialPageRoute(builder: (_) => CardType());
      case '/paymentPage':
        return MaterialPageRoute(builder: (_) => PaymentPage());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                child: Text('No route defined for ${settings.name}'),
              ),
            ));
    }
  }
}